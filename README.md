## This folder includes files and information access libre to public   
----

#### List of folders:  
- **CompetencyQuestions**: this folder contains a set of competency questions   
- **Testing**: this folder contains all unit-test files for the DSS.  
- **Rule**: this folder contains all the rules for the DSS.  
- **Sample**: this folder contains a data sample model by CASO and IRRIG.  

#### Abbreviations:  
- **CG**: Crop Growth  
- **RI**: Rain Intensity  
- **RM**: Root Zone Moisture Level  
- **CWN**: Crop Water Need  
- **SD**: Sleeping Duration  
